//
//  ViewController.m
//  CalenderTapkuJumartin
//
//  Created by Muhammad Umair Qureshi on 8/17/16.
//  Copyright © 2016 Muhammad Umair Qureshi. All rights reserved.
//

#import "ViewController.h"
#import "NSDate+TKCategory.h"


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    
    
    self.title = @"Testing";
    CGRect frame = self.view.frame;
    frame.size.width = [UIDevice phoneIdiom] ? CGRectGetWidth(self.view.frame) : 320;
    self.view.frame = frame;
    
    if([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    self.dayView = [[TKCalendarDayView alloc] initWithFrame:self.view.bounds];
    self.dayView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.dayView.delegate = self;
    self.dayView.dataSource = self;
    [self.view addSubview:self.dayView];
    
    mutArrEvents = [NSMutableArray arrayWithObjects:
                    @[@"Meeting with CEO, MD and COO", @"Paresh Navadiya Paresh Navadiya", @2, @0, @10, @15],
                    @[@"Call with HCA Client, Call with HCA Client, Call with HCA Client", @"Paresh Navadiya", @7, @0, @7, @45],
                    @[@"Break for 1 hour", @"Paresh Navadiya", @15, @0, @16, @00],
                    @[@"Break for 1 hour and 30 minutes", @"Paresh Navadiya", @15, @0, @16, @30],
                    @[@"Reports for product managment", @"Paresh Navadiya", @5, @30, @6, @0],
                    @[@"QC Task needed to be done", @"Paresh Navadiya", @19, @30, @24, @0], nil];
    
    
    mutArrEventsTomorrow = [NSMutableArray arrayWithObjects:
                            @[@"Meeting with CEO, MD", @"Paresh Navadiya", @2, @0, @10, @15],
                            @[@"Call with HCA Client, Call with HCA Client, Call with HCA Client", @"Paresh Navadiya", @7, @0, @7, @45],
                            @[@"Break for 1 hour", @"Waqas", @15, @0, @16, @00],
                            @[@"Break for 1 hour and 30 minutes", @"Paresh Navadiya", @15, @0, @16, @30],
                            @[@"Reports for product managment", @"Umair Qureshi", @5, @30, @6, @0],
                            @[@"QC Task needed to be done", @"Zain", @19, @30, @24, @0], nil];
    
    
    
    mutArrEventsYesterday = [NSMutableArray arrayWithObjects:
                             @[@"Yesterday Meeting with CEO, MD", @"Paresh Navadiya", @2, @0, @10, @15],
                             @[@"Yesterday Call with HCA Client, Call with HCA Client, Call with HCA Client", @"Paresh Navadiya", @7, @0, @7, @45],
                             @[@"Yesterday Break for 1 hour", @"Waqas", @15, @0, @16, @00],
                             @[@"Yesterday Break for 1 hour and 30 minutes", @"Paresh Navadiya", @15, @0, @16, @30],
                             @[@"Yesterday Reports for product managment", @"Umair Qureshi", @5, @30, @6, @0],
                             @[@"v QC Task needed to be done", @"Zain", @19, @30, @24, @0], nil];
    
    /* CGRect frameDayView = self.view.bounds;
     frameDayView.origin.y = 66;
     frameDayView.size.height -= 66;
     
     self.dayView.frame = frameDayView; */
    
    //paresh
    NSDateComponents *compNow = [NSDate componentsOfCurrentDate];
    [self performSelector:@selector(updateToCurrentTime) withObject:self afterDelay:60.0f-compNow.second];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)updateToCurrentTime
{
    if (self.dayView) {
        [self.dayView.nowLineView updateTime];
    }
    [self.dayView.fajrLineView updateTimeFajr];
    [self performSelector:@selector(updateToCurrentTime) withObject:self afterDelay:60.0f];
}


#pragma mark TKCalendarDayViewDelegate
- (NSArray *) calendarDayTimelineView:(TKCalendarDayView*)calendarDayTimeline eventsForDate:(NSDate *)eventDate{
    
    NSLog(@"eventDate : %@",eventDate);
    
    NSLog(@"[NSDate date] : %@",[NSDate date]);
    
    NSDateComponents *info = [[NSDate date] dateComponentsWithTimeZone:calendarDayTimeline.calendar.timeZone];
    
    NSLog(@"info %@",info);
    
    NSDateComponents *infoEvents = [eventDate dateComponentsWithTimeZone:calendarDayTimeline.calendar.timeZone];
    
    NSLog(@"infoEvents %@",infoEvents);
    
    NSLog(@"calendarDayTimeline %@",calendarDayTimeline.calendar.timeZone);
    
    
    if ([eventDate compare:[NSDate date]] == NSOrderedSame)    {
        NSLog(@"Today date");
    }
    if([eventDate compare:[NSDate dateWithTimeIntervalSinceNow:-24*60*60]] == NSOrderedAscending)
    {
        NSLog(@"NSOrderedAscending");
        //   return @[];
        
        infoEvents.second = 0;
        NSMutableArray *ret1 = [NSMutableArray array];
        
        for(NSArray *ar in mutArrEventsYesterday){
            NSLog(@"array ar %@",ar);
            
            
            TKCalendarDayEventView *event = [calendarDayTimeline dequeueReusableEventView];
            if(event == nil) event = [TKCalendarDayEventView eventView];
            
            NSInteger col = arc4random_uniform(3);
            [event setColorType:col];
            
            event.identifier = nil;
            event.titleLabel.text = ar[0];
            event.locationLabel.text = ar[1];
            
            // event start time
            infoEvents.hour = [ar[2] intValue];
            infoEvents.minute = [ar[3] intValue];
            event.startDate = [NSDate dateWithDateComponents:infoEvents];
            
            
            //event end time
            infoEvents.hour = [ar[4] intValue];
            infoEvents.minute = [ar[5] intValue];
            event.endDate = [NSDate dateWithDateComponents:infoEvents];
            
            [ret1 addObject:event];
            
        }
        
        return ret1;
    }
    
    if([eventDate compare:[NSDate dateWithTimeIntervalSinceNow:24*60*60]] == NSOrderedDescending)
    {
        NSLog(@"NSOrderedDescending");
        
        
        infoEvents.second = 0;
        NSMutableArray *ret1 = [NSMutableArray array];
        
        for(NSArray *ar in mutArrEventsTomorrow){
            NSLog(@"array ar %@",ar);
            
            
            TKCalendarDayEventView *event = [calendarDayTimeline dequeueReusableEventView];
            if(event == nil) event = [TKCalendarDayEventView eventView];
            
            NSInteger col = arc4random_uniform(3);
            [event setColorType:col];
            
            event.identifier = nil;
            event.titleLabel.text = ar[0];
            event.locationLabel.text = ar[1];
            
            // event start time
            infoEvents.hour = [ar[2] intValue];
            infoEvents.minute = [ar[3] intValue];
            event.startDate = [NSDate dateWithDateComponents:infoEvents];
            
            
            //event end time
            infoEvents.hour = [ar[4] intValue];
            infoEvents.minute = [ar[5] intValue];
            event.endDate = [NSDate dateWithDateComponents:infoEvents];
            
            [ret1 addObject:event];
            
        }
        
        
        return ret1;
        
        //  return @[];
    }
    
    
    info.second = 0;
    NSMutableArray *ret = [NSMutableArray array];
    
    for(NSArray *ar in mutArrEvents){
        NSLog(@"array ar %@",ar);
        
        
        TKCalendarDayEventView *event = [calendarDayTimeline dequeueReusableEventView];
        if(event == nil) event = [TKCalendarDayEventView eventView];
        
        NSInteger col = arc4random_uniform(3);
        [event setColorType:col];
        
        event.identifier = nil;
        event.titleLabel.text = ar[0];
        event.locationLabel.text = ar[1];
        
        // event start time
        info.hour = [ar[2] intValue];
        info.minute = [ar[3] intValue];
        event.startDate = [NSDate dateWithDateComponents:info];
        
        
        //event end time
        info.hour = [ar[4] intValue];
        info.minute = [ar[5] intValue];
        event.endDate = [NSDate dateWithDateComponents:info];
        
        [ret addObject:event];
        
    }
    return ret;
    
    
}

-(void)setEventsOnDate
{
    
}
- (void) calendarDayTimelineView:(TKCalendarDayView*)calendarDayTimeline eventViewWasSelected:(TKCalendarDayEventView *)eventView{
    NSLog(@"events seleted--->%@",eventView.titleLabel.text);
    
    
}

@end
