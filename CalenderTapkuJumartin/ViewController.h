//
//  ViewController.h
//  CalenderTapkuJumartin
//
//  Created by Muhammad Umair Qureshi on 8/17/16.
//  Copyright © 2016 Muhammad Umair Qureshi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TKCalendarDayViewController.h"
#import "TKCalendarDayEventView.h"
#import "TKCalendarDayView.h"
#import "UIDevice+TKCategory.h"
@interface ViewController : UIViewController<TKCalendarDayViewDelegate,TKCalendarDayViewDataSource>

{
    NSMutableArray *mutArrEvents;
    NSMutableArray *mutArrEventsTomorrow;
    NSMutableArray *mutArrEventsYesterday;
    
}

/** Returns the day view managed by the controller object. */
@property (nonatomic,strong) TKCalendarDayView *dayView;
@end

