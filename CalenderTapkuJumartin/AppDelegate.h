//
//  AppDelegate.h
//  CalenderTapkuJumartin
//
//  Created by Muhammad Umair Qureshi on 8/17/16.
//  Copyright © 2016 Muhammad Umair Qureshi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

