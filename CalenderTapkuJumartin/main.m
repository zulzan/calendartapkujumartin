//
//  main.m
//  CalenderTapkuJumartin
//
//  Created by Muhammad Umair Qureshi on 8/17/16.
//  Copyright © 2016 Muhammad Umair Qureshi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
